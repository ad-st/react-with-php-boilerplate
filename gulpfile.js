const   gulp = require('gulp'),
        php = require('gulp-connect-php'),
        browserSync = require('browser-sync').create(),
        browserify = require('browserify'),
        source = require('vinyl-source-stream');

const config = {
    port: 8080,
    phpPort: 8010,
    paths: {
        php: './src/*.php',
        js: './src/*.js',
        dist: './dist',
        mainJs: "./src/main.js"
    }
}

gulp.task('php-server', () => {
    php.server({ base: config.paths.dist, port: config.phpPort, keepalive: true});
});

gulp.task('browser-sync',['php-server'], () => {
    browserSync.init({
        proxy: '127.0.0.1:' + config.phpPort,
        port: config.port,
        open: true,
        notify: false,
    });
});

gulp.task('php', () => {
    gulp.src(config.paths.php)
        .pipe(gulp.dest(config.paths.dist))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('js', () => {
    browserify(config.paths.mainJs)
        .transform("babelify", {presets: ["@babel/preset-env", "@babel/preset-react"]})
        .bundle()
        .on('error', console.error.bind(console))
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(config.paths.dist + '/scripts'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('watch', () => {
    gulp.watch([config.paths.php], ['php']);
    gulp.watch([config.paths.js], ['js']);
});

gulp.task('default', ['php', 'js', 'browser-sync', 'watch']);